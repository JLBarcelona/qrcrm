<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Item extends Authenticatable
{

  protected $primaryKey = 'item_id';

   public $timestamps = false;

   protected $table = "fcrm_items";

   protected $appends = ['result_user','main_form_name'];

   protected $fillable = [
        'company_id','main_form_id', 'qty','price', 'sale_price', 'is_sale', 'date_created', 'date_modified', 'date_deleted'
    ];

	   public function mainform()
    {
        return $this->belongsTo('App\MainForm', 'main_form_id');
    }

    public function result()
    {
        return $this->hasMany('App\Result', 'item_id');
    }

    public function getResultUserAttribute(){
      return $this->result()->get();
    }

    public function getMainFormNameAttribute(){
      $result = $this->mainform()->firstOrFail();

      return $result->main_form_name;
    }

    public function getResult(){
      return $this->result();
    }

    public function getForms(){
      return $this->mainform()->get();
    }

    public function purchased()
    {
        return $this->hasMany('App\Purchased', 'item_id');
    }


}
