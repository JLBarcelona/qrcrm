<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class FormTake extends Authenticatable
{

  protected $primaryKey = 'form_build_result_id';

   public $timestamps = false;

   protected $table = "fcrm_form_take";

   protected $appends = ['forms'];

   protected $fillable = [
        'slug','form_build_id','takeable_id','takeable_type','take_data','date_finish','date_created','date_deleted'
    ];

    public function getFormsAttribute(){
      return $this->form_build()->first();
    }

    public function takeable(){
      return $this->morphTo();
    }

    public function getDateFinishAttribute($value)
    {
        // return ucfirst($value);
        // return  \Carbon\Carbon::parse($value)->format('m-d-y h:i:s A');

        if (!empty($value)) {
          return  \Carbon\Carbon::parse($value)->format('jS F Y h:i:s A');
        }else{
          return null;
        }

    }

    public function form_build(){
     return $this->belongsTo('App\FormBuild', 'form_build_id');
    }

}
