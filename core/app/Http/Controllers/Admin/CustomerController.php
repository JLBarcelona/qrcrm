<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Customer;

class CustomerController extends Controller {

  function list_customer(){
  	$customer = Customer::whereNull('deleted_at')->get();
  	return response()->json(['status' => true, 'data' => $customer]);
  }

  function add_customer(Request $request){
  	$customer_id= $request->get('customer_id');
  	$profile_picture = $request->get('profile_picture');
  	$fullname = $request->get('fullname');
  	$designation = $request->get('designation');
  	$customer_company = $request->get('customer_company');
  	$mobile_number = $request->get('mobile_number');
  	$address = $request->get('address');
  	$country = $request->get('country');
  	$postal_code = $request->get('postal_code');

  	$validator = Validator::make($request->all(), [
  		// 'profile_picture' => 'required',
  		'fullname' => 'required',
  		'designation' => 'required',
  		'customer_company' => 'required',
  		'mobile_number' => 'required',
  		'address' => 'required',
  		'country' => 'required',
  		'postal_code' => 'required',
  	]);

  	if ($validator->fails()) {
  		return response()->json(['status' => false, 'error' => $validator->errors()]);
  	}else{
  		if (!empty($customer_id)) {
  			$customer = Customer::find($customer_id);
  			// $customer->profile_picture = $profile_picture;
  			$customer->fullname = $fullname;
  			$customer->designation = $designation;
  			$customer->customer_company = $customer_company;
  			$customer->mobile_number = $mobile_number;
  			$customer->address = $address;
  			$customer->country = $country;
  			$customer->postal_code = $postal_code;
  			if($customer->save()){
  				return response()->json(['status' => true, 'message' => 'Customer updated successfully!']);
  			}
  		}else{
  			$customer = new Customer;
  			// $customer->profile_picture = $profile_picture;
  			$customer->fullname = $fullname;
  			$customer->designation = $designation;
  			$customer->customer_company = $customer_company;
  			$customer->mobile_number = $mobile_number;
  			$customer->address = $address;
  			$customer->country = $country;
  			$customer->postal_code = $postal_code;
  			if($customer->save()){
  				return response()->json(['status' => true, 'message' => 'Customer saved successfully!']);
  			}
  		}
  	}
  }


  function delete_customer($customer_id){
  	$customer = Customer::find($customer_id);
  	if($customer->delete()){
  		return response()->json(['status' => true, 'message' => 'Customer deleted successfully!']);
  	}
  }
}
