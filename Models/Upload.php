<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Upload extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $primaryKey = 'upload_id';
    protected $table = 'fcrm_upload';

    protected $fillable = [
        'file_name', 'file_type', 'user_id', 'company_id', 'product_id', 'upload_type','date_entry','date_modified','date_deleted'
    ];

    // UPLOAD TYPE = 1 = gallery, 2 = attachment


    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

}
