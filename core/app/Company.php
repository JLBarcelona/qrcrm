<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;
use Url;
class Company extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'company_id';
    protected $table = 'qrcrm_company';

    protected $appends = ['qrcode_detail', 'qrcode'];

    protected $fillable = [
        'slug', 'company_logo', 'company_name', 'address_line_1','address_line_2','city','postal_code','country','industry','website','facebook_link','office_number','created_from','user_id','created_at','updated_at','deleted_at'
    ];


    public function getQrcodeDetailAttribute(){
      // $data = "tel:$this->office_number";
      $data = '';
      $data .= 'BEGIN:VCARD'."\n";
      $data .= 'VERSION:3.0'."\n";
      $data .= 'FN:'.$this->company_name."\n";
      $data .= 'TITLE:'.$this->industry."\n";
      $data .= 'EMAIL:'."\n";
      $data .= 'TEL:'.$this->mobile."\n";
      $data .= 'TEL:'.$this->office_number."\n";
      $data .= 'ADDR:'.$this->address_line_1.' '.$this->country.' '.$this->postal_code."\n";
      $data .= 'URL:'.$this->facebook_link."\n";
      $data .= 'URL:'.$this->instagram_link."\n";
      $data .= 'URL:'.$this->twitter_link."\n";
      $data .= 'URL:'.$this->website."\n";
      $data .= 'END:VCARD'."\n";


      return 'data:image/svg+xml;base64, '.base64_encode(\QrCode::size(99)->generate($data));
      // return ;
    }

     public function getCreatedAttribute($value){
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }

    public function getQrcodeAttribute(){
      $url = url('Details/').$this->slug;
      return 'data:image/svg+xml;base64, '.base64_encode(\QrCode::size(99)->generate('Testing'));
    }

    public function owner(){
      return $this->morphOne('App\User', 'owner')->whereNull('deleted_at');
    }

    // public function getAccountAttribute(){
    //   return $this->owner;
    // }



}
