<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'prod_category_id';
    protected $table = 'qrcrm_category';

    protected $fillable = [
      'category','created_at','updated_at','deleted_at','owner_type','owner_id'
    ];
}
