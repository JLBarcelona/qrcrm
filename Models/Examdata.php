<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Examdata extends Authenticatable
{

  protected $primaryKey = 'exam_id';

   public $timestamps = false;

   protected $table = "fcrm_exam_info";

   protected $appends = ['choice_name'];

   protected $fillable = [
        'purchased_id', 'item_id', 'main_form_id', 'form_id', 'id', 'question_number','question_id','choice_id', 'score','date_answer'
    ];


    public function getChoiceNameAttribute(){
      return $this->choice()->first();
    }

     public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }

     public function choice()
    {
        return $this->belongsTo('App\Choice', 'choice_id');
    }

    public function GetCorrectAnswer(){
        return $this->choice()->get();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }
}
