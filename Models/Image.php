<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Image extends Authenticatable
{

   protected $primaryKey = 'image_id';


   public $timestamps = false;

   protected $table = "fcrm_image";

   protected $fillable = [
        'question_id', 'file_name', 'file_path', 'date_uploaded'
    ];


    public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }
}
