<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logo.png') ])
<body class="">
  <div class="">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
    <div class="">
      <section class="page-content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
                @include('Layout.bread', ['data' => [ 'Customer' => url()->current() ] ])
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-header main-headers">
                      <div class="title">Customer</div>
                      <div class="options">
                        <button class="btn btn-primary btn-sm" onclick="add_new_customer();"><i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                    <div class="card-body">
                      <table class="table table-bordered dt-responsive nowrap" id="tbl_customer" style="width: 100%;"></table>
                    </div>
                    <div class="card-footer"></div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>

<div class="modal fade" role="dialog" id="add_new_customer">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <div class="modal-title company">
             Add Customer
           </div>
           <button class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
           <form class="needs-validation" id="customer_form" action="{{ url('customer/add_customer') }}" novalidate>
            	<div class="form-row">
            		<input type="hidden" id="customer_id" name="customer_id" placeholder="" class="form-control" required>
            		<div class="form-group col-sm-12">
            			<label>Profile Picture </label>
            			<input type="file" id="profile_picture" name="profile_picture" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_profile_picture"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Fullname </label>
            			<input type="text" id="fullname" name="fullname" placeholder="Full Name" class="form-control " required>
            			<div class="invalid-feedback" id="err_fullname"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Designation </label>
            			<input type="text" id="designation" name="designation" placeholder="Designation" class="form-control " required>
            			<div class="invalid-feedback" id="err_designation"></div>
            		</div>
            		<div class="form-group col-sm-12">
                  <label>Customer Company </label>
            			<select id="customer_company" name="customer_company" class="form-control ">
                    <option value="">Select Customer Company</option>
                    <option value="Test 1">Test 1</option>
                    <option value="Test 2">Test 2</option>
                  </select>
            			<div class="invalid-feedback" id="err_customer_company"></div>
            		</div>
            			<div class="form-group col-sm-12">
            			<label>Mobile Number </label>
            			<input type="number" id="mobile_number" name="mobile_number" placeholder="Mobile Number" class="form-control " required>
            			<div class="invalid-feedback" id="err_mobile_number"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Address </label>
            			<input type="text" id="address" name="address" placeholder="Address" class="form-control " required>
            			<div class="invalid-feedback" id="err_address"></div>
            		</div>
            		<div class="form-group col-sm-12">
                  <label>Country </label>
            			<select id="country" name="country" class="form-control ">
                    <option value="">Select Country</option>
                    <option value="Singapore">Singapore</option>
                    <option value="Philippines">Philippines</option>
                  </select>
            			<div class="invalid-feedback" id="err_country"></div>
            		</div>
            			<div class="form-group col-sm-12">
            			<label>Postal Code </label>
            			<input type="number" id="postal_code" name="postal_code" placeholder="Postal Code" class="form-control " required>
            			<div class="invalid-feedback" id="err_postal_code"></div>
            		</div>

            		<div class="col-sm-12 text-right">
            			<button class="btn btn-success" type="submit">Submit</button>
            		</div>
            	</div>
            </form>

         </div>
         <div class="modal-footer">

         </div>
    </div>
  </div>
</div>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<script type="text/javascript">
  function add_new_customer(){
    $("#add_new_customer").modal({'backdrop' : 'static'});
    $(".customer").text('Add Customer');
  }
</script>


<script>
	var tbl_customer;

  show_customer();

	function show_customer(){
		if (tbl_customer) {
			tbl_customer.destroy();
		}
		var url = main_path + '/customer/list_customer';
		tbl_customer = $('#tbl_customer').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "fullname",
		"title": "Fullname",
	},{
		className: '',
		"data": "designation",
		"title": "Designation",
	},{
		className: '',
		"data": "customer_company",
		"title": "Customer_company",
	},{
		className: '',
		"data": "mobile_number",
		"title": "Mobile_number",
	},{
		className: '',
		"data": "address",
		"title": "Address",
	},{
		className: '',
		"data": "country",
		"title": "Country",
	},{
		className: '',
		"data": "postal_code",
		"title": "Postal_code",
	},{
		className: 'width-option-1 text-center',
		"data": "customer_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-primary btn-sm font-base mt-1 mr-1" data-info=\' '+param_data.trim()+'\' onclick="edit_customer(this)" type="button"><i class="fa fa-list"></i> Details</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#customer_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'customer_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'customer_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_customer(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/customer/delete_customer/' + data.customer_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this customer?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_customer(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#customer_id').val(data.customer_id);
		$('#profile_picture').val(data.profile_picture);
		$('#fullname').val(data.fullname);
		$('#designation').val(data.designation);
		$('#customer_company').val(data.customer_company);
		$('#mobile_number').val(data.mobile_number);
		$('#address').val(data.address);
		$('#country').val(data.country);
		$('#postal_code').val(data.postal_code);
    $("#add_new_customer").modal({'backdrop' : 'static'});
    $(".customer").text('Edit Customer');
	}
</script>
