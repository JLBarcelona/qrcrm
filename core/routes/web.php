<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace('Account')->group(function () {
	Route::get('/', function () {
	    return view('Account.index');
	});

	Route::post('/check_user', 'AccountController@login_user');
	Route::get('/logout/admin','AccountController@logout_admin')->name('logout.admin');

	Route::get('/logout/user','AccountController@logout_user')->name('logout.user');

 	Route::get('/forgot_password', function(){
	    return view('Account.forgot_password');
	});

	Route::post('get_token/{user_type}', 'ForgotPasswordController@get_token');
	Route::get('/new_password/{token}/{id}', 'ForgotPasswordController@new_password')->name('account.new_password');
	Route::get('/verify_account/{token}/{id}', 'AccountController@verify_account')->name('account.verify');
	Route::post('/create_new_password/{token}/{id}', 'ForgotPasswordController@create_new_password');


	//Registration
	 Route::get('/register', 'AccountController@register_index')->name('profile.index');
	 Route::post('/register/add_register', 'AccountController@add_register')->name('register.add');
	 Route::post('/profile/add_profile', 'AccountController@add_profile')->name('profile.add');


});


Route::namespace('Admin')->group(function () {
	Route::middleware(['auth'])->group(function () {
		Route::get('/system/settings', 'AdminController@settings')->name('admin.settings');

		Route::get('/admin', 'AdminController@index')->name('admin.index');

		// Company
		Route::get('Admin/Company', function(){
			return view('Company.company_admin');
		})->name('Admin.Company');

		 Route::get('/comp/list_comp', 'CompanyController@list_comp')->name('comp.list');
		 Route::post('/comp/add_comp', 'CompanyController@add_comp')->name('comp.add');
		 Route::get('/comp/delete_comp/{company_id}', 'CompanyController@delete_comp')->name('comp.delete');

		// end company

		//Company User
		Route::get('Admin/User', function(){
			return view('User.user_admin');
		})->name('Admin.User');

		Route::get('/comp_user/list_comp_user', 'CompanyUserController@list_comp_user')->name('comp_user.list');
		Route::post('/comp_user/add_comp_user', 'CompanyUserController@add_comp_user')->name('comp_user.add');
		Route::get('/comp_user/delete_comp_user/{company_user_id}', 'CompanyUserController@delete_comp_user')->name('comp_user.delete');


		// end Company User

		// Customer

		Route::get('Admin/Customer', function(){
			return view('Customer.customer_admin');
		})->name('Admin.Customer');

		Route::get('/customer/list_customer', 'CustomerController@list_customer')->name('customer.list');
		Route::post('/customer/add_customer', 'CustomerController@add_customer')->name('customer.add');
		Route::get('/customer/delete_customer/{customer_id}', 'CustomerController@delete_customer')->name('customer.delete');

		// End Customer



	});
});
