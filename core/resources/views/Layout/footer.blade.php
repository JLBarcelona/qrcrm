<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/ajaxsetup.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>

@include('Layout.modal')


@if($type == 'home')

@elseif($type == 'user')

@elseif($type == 'admin')

@elseif($type == 'store')

@elseif($type == 'barangay')
<script src="{{ asset('asset/brgy/brgy.js') }}"></script>

@elseif($type == 'register')
<script src="{{ asset('asset/js/multistepform.js') }}"></script>

@endif


 <div class="modal fade" role="dialog" id="modal_preview_images">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title title_preview">
        Preview
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <img src="{{ asset('assets/img/img.png') }}" class="img_show_preview img-bg img-fluid" width="650" alt="">
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	setTimeout(function(){
		$(".img_preview").on('click', function(){
			let src = $(this).attr('src');
			let title = 'Preview';
			if (typeof $(this).attr('data-prev-title') == 'undefined') {
				title = $(this).attr('data-title');
			}else{
				title = 'Preview';
			}
			$(".title_preview").text(title);
			$('.img_show_preview').attr('style', 'background-image: url(\''+src+'\')');
			$("#modal_preview_images").modal('show');
		});
	}, 2000);
</script>