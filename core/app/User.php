<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'user_id';


    protected $table = 'qrcrm_user';

    protected $fillable = [
        'username','email_address','user_type','verified','confirm','created_at','updated_at','deleted_at'
    ];

    // user_type Guides
    // 1 = admin, 2 = barangay, 3 = store,  4 =profile

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'owner_id', 'owner_type'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function owner()
    {
        return $this->morphTo();
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }


}
