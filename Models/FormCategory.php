<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class FormCategory extends Authenticatable
{

  protected $primaryKey = 'category_id';

   public $timestamps = false;

   protected $table = "fcrm_category";

   protected $appends = ['details'];

   protected $fillable = [
        'form_id', 'category_name','date_entry', 'date_modified', 'date_deleted'
    ];

    public function getDetailsAttribute(){
      return $this->question()->get();
    }

    public function question()
    {
        return $this->hasMany('App\Question', 'category_id');
    }

     public function form()
    {
        return $this->hasMany('App\Form', 'category_id');
    }

    public function getCategoryQuestion(){
      $res = $this->question()->get();
      return $res;
    }

    public function getCategoryQuestionForm(){
      $res = $this->question();
      return $res;
    }

   
}
