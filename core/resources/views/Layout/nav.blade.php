@if($type == 'home')

@elseif($type == 'user')

@elseif($type == 'admin')
 <!-- Navbar -->
  <nav class="main-headers navbar theme-nav navbar-expand-lg navbar-primary navbar-dark">
    <!-- Left navbar links -->
    <a class="navbar-brand bold th-fs" href="#">QRCRM</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
     
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
         <li class="nav-item">
          <a class="nav-link" href="{{ url('Admin/Company') }}" role="button"><i class="fas fa-building"></i> Company</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('Admin/User') }}" role="button"><i class="fas fa-user-tie"></i> User</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('Admin/Customer') }}" role="button"><i class="fas fa-users"></i> Customer</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-user-circle"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">{{ Auth::user()->owner->admin_name }}</span>
            <div class="dropdown-divider"></div>
            <a href="{{ url('admin/accountsetting') }}" class="dropdown-item">
              <i class="fas fa-shield-alt mr-2"></i> Account Settings
            </a>
            <div class="dropdown-divider"></div>
              <a href="{{ url('/logout/user') }}" class="dropdown-item">
              <i class="fas fa-sign-out-alt"></i> Logout
            </a>
          </div>
        </li>
      </ul>
    </div>
     
  </nav>
@elseif($type == 'store')
  
@elseif($type == 'barangay')
   
@endif
