<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

// Models
use App\CompanyUser;
use App\User;

// datatables
use DB;
use Yajra\Datatables\Datatables;

class CompanyUserController extends Controller {

  function list_comp_user(){
	$comp_user = CompanyUser::with('owner')->whereNull('deleted_at')->get();
	// return response()->json(['status' => true, 'data' => $comp_user]);
  return datatables()->of($comp_user)->make(true);
  
  }

  function add_comp_user(Request $request){
  	$company_user_id= $request->get('company_user_id');
  	$profile_picture = $request->get('profile_picture');
  	$fullname = $request->get('fullname');
  	$username = $request->get('username');
  	$designation = $request->get('designation');
  	$company_id = $request->get('company_id');
  	$email_address = $request->get('email_address');
  	$mobile_number = $request->get('mobile_number');
  	$address = $request->get('address');
  	$country = $request->get('country');
  	$postal_code = $request->get('postal_code');
  	$facebook_link = $request->get('facebook_link');
  	$instagram_link = $request->get('instagram_link');
  	$twitter_link = $request->get('twitter_link');

  	$validator = Validator::make($request->all(), [
  		// 'profile_picture' => 'required',
  		'fullname' => 'required',
  		'username' => 'required',
  		'designation' => 'required',
  		'company_id' => 'required',
  		'email_address' => 'required',
  		'mobile_number' => 'required',
  		'address' => 'required',
  		'country' => 'required',
  		'postal_code' => 'required',
  		'facebook_link' => 'required',
  		'instagram_link' => 'required',
  		'twitter_link' => 'required',
  	]);

  	if ($validator->fails()) {
  		return response()->json(['status' => false, 'error' => $validator->errors()]);
  	}else{
  		if (!empty($company_user_id)) {
  			$comp_user = CompanyUser::find($company_user_id);
  			// $comp_user->profile_picture = $profile_picture;
  			$comp_user->fullname = $fullname;
  			$comp_user->username = $username;
  			$comp_user->designation = $designation;
  			$comp_user->company_id = $company_id;
  			$comp_user->email_address = $email_address;
  			$comp_user->mobile_number = $mobile_number;
  			$comp_user->address = $address;
  			$comp_user->country = $country;
  			$comp_user->postal_code = $postal_code;
  			$comp_user->facebook_link = $facebook_link;
  			$comp_user->instagram_link = $instagram_link;
  			$comp_user->twitter_link = $twitter_link;
  			if($comp_user->save()){
  				return response()->json(['status' => true, 'message' => 'Comp_user updated successfully!']);
  			}
  		}else{
  			$comp_user = new CompanyUser;
  			// $comp_user->profile_picture = $profile_picture;
  			$comp_user->fullname = $fullname;
  			$comp_user->designation = $designation;
  			$comp_user->company_id = $company_id;
  			$comp_user->mobile_number = $mobile_number;
  			$comp_user->address = $address;
  			$comp_user->country = $country;
  			$comp_user->postal_code = $postal_code;
  			$comp_user->facebook_link = $facebook_link;
  			$comp_user->instagram_link = $instagram_link;
  			$comp_user->twitter_link = $twitter_link;
  			if($comp_user->save()){
          // Add User
					$user = new User;
					$user->username = $username;
					$user->email_address = $email_address;
					$user->password = $fullname;
					$user->owner_type = get_class($comp_user);
					$user->owner_id = $comp_user->company_user_id;
					if ($user->save()) {
						return response()->json(['status' => true, 'message' => 'Company User saved successfully!']);
					}
  			}
  		}
  	}
  }


    function delete_comp_user($company_user_id){
  	$comp_user = CompanyUser::find($company_user_id);
  	if($comp_user->delete()){
  		return response()->json(['status' => true, 'message' => 'Comp_user deleted successfully!']);
  	}
  }
}
