<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\DailyAttendance;
use App\Payroll;
use App\User;
use App\LateConfig;
use App\PayrollDetail;


use Carbon\Carbon;

class DetailExcel implements ToCollection
{
    /**
    * @param Collection $collection
    */

    public function collection(Collection $collection)
    {
        $owner_id = '';
        $owner_type = '';
        
        // dd($collection);
        foreach ($collection as $key => $value) {

            if ($key == 1) {
                foreach ($value as $k => $v) {
                    if ($k == 1) {
                        $detail = explode(' ~ ', $v);
                        $start = $detail[0];
                        $end = $detail[1];

                        $payroll_check = Payroll::where('report_from', $start)->where('report_to', $end);

                        if ($payroll_check->count() > 0) {
                            $pay = $payroll_check->first();
                            $owner_id = $pay->payroll_id;
                            $owner_type = get_class($pay);
                        }else{
                            $payroll = new Payroll;
                            $payroll->report_from = $start;
                            $payroll->report_to = $end;
                            $payroll->save();
                            $owner_id = $payroll->payroll_id;
                            $owner_type = get_class($payroll);
                        }
                    }
                }
            }


            if ($key > 4) {
                  $check_user = User::where('attendance_id', $value[0])->whereNull('deleted_at')->count();
                if ($check_user > 0) {
                    $detail = new PayrollDetail;
                    $detail->attendance_id = $value[0];
                    $detail->late_times = $value[5];
                    $detail->late_mins = $value[6];
                    $detail->absent_day = $value[13];
                    $detail->leave_early_times = $value[7];
                    $detail->leave_early_mins = $value[8];
                    // $detail->holiday_work = $value[10];
                    $days = explode('/', $value[11]);
                    $detail->attend_days = $days[0];
                    $detail->owner_id = $owner_id;
                    $detail->owner_type = $owner_type;

                    $detail->save();
                }
            }
        }

    }

}
