<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Form extends Authenticatable
{

   protected $primaryKey = 'form_id';


   public $timestamps = false;

   protected $table = "fcrm_form";

   protected $appends = ['details', 'category', 'question_number'];

   protected $fillable = [
        'company_id','form_name', 'date_created', 'date_deleted'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


     // Accesor

    public function getDetailsAttribute(){
      // $limit = $this->question_number;
      return $this->question();
    }

    public function getCategoryAttribute(){
      return $this->category()->get();
    }

     // Accesor


      public function question()
    {
        return $this->hasMany('App\Question', 'form_id');
    }

    public function category()
    {
        return $this->hasMany('App\FormCategory', 'form_id');
    }

    public function getCategoryForm(){
        $res = $this->category()->get();
        return $res;
    }

    public function getCategoryFormquestion(){
        $res = $this->category()->has('question')->get();
        return $res;
    }

     public function formdetail()
    {
        return $this->hasMany('App\FormDetail', 'form_id');
    }

     public function result()
    {
        return $this->hasMany('App\Result', 'form_id');
    }


       public function order()
    {
        return $this->hasMany('App\FormOrder', 'main_form_id');
    }

}
