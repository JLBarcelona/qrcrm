<?php
namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AttendanceImport implements WithMultipleSheets 
{
   
    public function sheets(): array
    {
        return [
            1 => new DetailExcel(),
            2 => new MonthlyExcel(),
        ];
    }
}

