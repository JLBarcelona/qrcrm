<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Result extends Authenticatable
{

  protected $primaryKey = 'result_id';

   public $timestamps = false;

   protected $table = "fcrm_exam_result";

   protected $appends = ['user_detail', 'user_purchased'];


   protected $fillable = [
        'purchased_id', 'id', 'item_id', 'total_score','is_finish', 'total_question_number', 'passing_mark', 'date_finish'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserDetailAttribute(){
      return $this->user()->first();
    }

    public function getUserPurchasedAttribute(){
      return $this->purchased()->first();
    }

    public function form()
    {
        return $this->hasMany('App\Form', 'form_id');
    }

     public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }

    public function items()
   {
       return $this->belongsTo('App\Item', 'item_id');
   }

   public function purchased()
   {
       return $this->belongsTo('App\Purchased', 'purchased_id');
   }
}
