<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logo.png') ])
<body class="">
  <div class="">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
    <div class="">
      <section class="page-content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
                @include('Layout.bread', ['data' => [ 'User' => url()->current() ] ])
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-header main-headers">
                      <div class="title">User</div>
                      <div class="options">
                        <button class="btn btn-primary btn-sm" onclick="add_new_company_user();"><i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                    <div class="card-body">
                      <table class="table table-bordered dt-responsive nowrap" id="tbl_comp_user" style="width: 100%;"></table>
                    </div>
                    <div class="card-footer"></div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>

<div class="modal fade" role="dialog" id="add_new_company_user">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <div class="modal-title company">

           </div>
           <button class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
           <form class="needs-validation" id="company_user_form" action="{{ url('comp_user/add_comp_user') }}" novalidate>
            	<div class="form-row">
            		<input type="hidden" id="company_user_id" name="company_user_id" placeholder="" class="form-control" required>
                <div class="form-group col-sm-12">
            			<label>Profile Picture </label>
            			<input type="file" id="profile_picture" name="profile_picture" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_profile_picture"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Fullname </label>
            			<input type="text" id="fullname" name="fullname" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_fullname"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Username </label>
            			<input type="text" id="username" name="username" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_username"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Designation </label>
            			<input type="text" id="designation" name="designation" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_designation"></div>
            		</div>
            		<div class="form-group col-sm-12">
                  <label>Company</label>
              			<div class="form-group col-sm-">
                			<select id="company_id" name="company_id" class="form-control ">
                        <option value="">Select Company</option>
                        <option value=1>Company Test 1</option>
                        <option value=2>Company Test 2</option>
                      </select>
                			<div class="invalid-feedback" id="err_company_id"></div>
              		  </div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Email Address </label>
            			<input type="text" id="email_address" name="email_address" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_email_address"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Mobile Number </label>
            			<input type="text" id="mobile_number" name="mobile_number" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_mobile_number"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Address </label>
            			<input type="text" id="address" name="address" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_address"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Country </label>
            			<input type="text" id="country" name="country" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_country"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Postal Code </label>
            			<input type="number" id="postal_code" name="postal_code" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_postal_code"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Facebook Link </label>
            			<input type="text" id="facebook_link" name="facebook_link" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_facebook_link"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Instagram Link </label>
            			<input type="text" id="instagram_link" name="instagram_link" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_instagram_link"></div>
            		</div>
            		<div class="form-group col-sm-12">
            			<label>Twitter Link </label>
            			<input type="text" id="twitter_link" name="twitter_link" placeholder="" class="form-control " required>
            			<div class="invalid-feedback" id="err_twitter_link"></div>
            		</div>

                <div class="col-sm-12 text-right">
                  <button class="btn btn-dark btn-sm" type="submit">Save</button>
                </div>
            	</div>
            </form>
         </div>
         <div class="modal-footer">

         </div>
       </div>
     </div>
   </div>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<script type="text/javascript">
  function add_new_company_user(){
    $("#add_new_company_user").modal({'backdrop' : 'static'});
    $(".company").text('Add Company User');
  }
</script>

<script>

	var tbl_comp_user;

  show_comp_user();

	function show_comp_user(){
		if (tbl_comp_user) {
			tbl_comp_user.destroy();
		}
		var url = main_path + '/comp_user/list_comp_user';
		tbl_comp_user = $('#tbl_comp_user').DataTable({
		pageLength: 10,
		responsive: true,
    serverSide: true,
    processing: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "fullname",
		"title": "Fullname",
	},{
		className: '',
		"data": "owner.username",
		"title": "Username",
	},{
		className: '',
		"data": "owner.email_address",
		"title": "Email Address",
	},{
		className: 'width-1 text-center',
		"data": "company_user_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
        console.log(row);
				newdata = '';
        newdata += '<button class="btn btn-info btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_comp(this)" type="button"><i class="fa fa-list"></i> Full Details</button>';
        newdata += ' <button class="btn btn-primary btn-sm font-base mt-1 mr-1" data-info=\' '+param_data.trim()+'\' onclick="edit_comp(this)" type="button"><i class="fa fa-list"></i> Reports</button>';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1 mr-1" data-info=\' '+param_data.trim()+'\' onclick="edit_comp_user(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_comp_user(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#company_user_form").on('submit', function(e){

		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					// console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'company_user_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'company_user_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_comp_user(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/comp_user/delete_comp_user/' + data.company_user_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this comp_user?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_comp_user(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#company_user_id').val(data.company_user_id);
		$('#profile_picture').val(data.profile_picture);
		$('#fullname').val(data.fullname);
		$('#username').val(data.owner.username);
		$('#designation').val(data.designation);
		$('#company_id').val(data.company_id);
		$('#email_address').val(data.owner.email_address);
		$('#mobile_number').val(data.mobile_number);
		$('#address').val(data.address);
		$('#country').val(data.country);
		$('#postal_code').val(data.postal_code);
		$('#facebook_link').val(data.facebook_link);
		$('#instagram_link').val(data.instagram_link);
		$('#twitter_link').val(data.twitter_link);
    $("#add_new_company_user").modal({'backdrop' : 'static'});
    $(".company").text('Edit Company');
	}
</script>
