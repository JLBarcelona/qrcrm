<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Permission extends Authenticatable
{

  protected $primaryKey = 'permission_id';

   public $timestamps = false;

   protected $table = "fcrm_permission";

   protected $fillable = [
        'company_id','crm','form_survey'
    ];


    public function permission()
   {
       return $this->belongsTo('App\Company', 'company_id');
   }

}
