<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class FormBuild extends Authenticatable
{

  protected $primaryKey = 'form_build_id';

   public $timestamps = false;

   protected $table = "fcrm_form_build";

   protected $appends = ['total_takes'];

   protected $fillable = [
        'formable_id','formable_type','form_name','form_data','is_template','date_created','date_delete'
    ];

    public function formable(){
      return $this->morphTo();
    }

    public function getTotalTakesAttribute(){
      return $this->form_take()->whereNotNull('date_finish')->count();
    }


    public function form_take(){
     return $this->hasMany('App\FormTake', 'form_build_id');
    }

    public function getDateCreatedAttribute($value)
    {
        // return ucfirst($value);
        // return  \Carbon\Carbon::parse($value)->format('m-d-y h:i:s A');
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }

}
