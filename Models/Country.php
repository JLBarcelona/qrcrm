<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Country extends Authenticatable
{

  protected $primaryKey = 'country_id';

   public $timestamps = false;

   protected $table = "fcrm_country";

   protected $fillable = [
        'name','code','phone_cody'
    ];
}
