<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'product_id';
    protected $table = 'qrcrm_product';

    protected $fillable = [
      'product_name','product_price','description','category_id','created_at','updated_at','deleted_at'
    ];

    protected $hidden = [ 'owner_type','owner_id' ];

    public function owner()
    {
        return $this->morphTo();
    }

    public function upload(){
  		return $this->morphOne('App\Upload', 'owner')->whereNull('deleted_at');
  	}
}
