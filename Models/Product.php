<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Product extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $primaryKey = 'product_id';
    protected $table = 'fcrm_product';


    protected $fillable = [
        'company_id','user_id', 'product_name', 'price', 'main_image','description','category', 'product_slug_id','image_sync','added_from','date_entry','date_modified','date_deleted'
    ];

    protected $appends = ['my_company', 'my_category'];

    public function getMyCompanyAttribute(){
      $data = $this->company()->first();
      return $data['company_name'];
    }

    public function getMyCategoryAttribute(){
      $data = $this->category()->first();
      return $data['category_name'];
    }

      public function company(){
        return $this->belongsTo('App\Company', 'company_id');
    }

      public function upload(){
        return $this->hasMany('App\Upload', 'product_id');
    }

    public function category(){
        return $this->belongsTo('App\Category', 'category', 'category_id');
    }


}
