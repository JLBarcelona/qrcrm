<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Log extends Authenticatable
{

   protected $primaryKey = 'log_id';

   protected $table = "fcrm_logs";

   protected $fillable = [
        'log','status','type','created_at','updated_at','deleted_at'
    ];

}
