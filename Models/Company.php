<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;

class Company extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $primaryKey = 'company_id';
    protected $table = 'fcrm_company';

    protected $appends = ['permission', 'qrcode_detail'];

    protected $fillable = [
        'qr_path', 'company_username', 'company_name', 'email_address', 'password', 'company_logo', 'address_line_1','address_line_2','city','postal_code','country','industry','website','facebook_link','office_number','created_from','user_id','date_entry','date_modified','date_deleted'
    ];

    public function getQrcodeDetailAttribute(){
      // $data = "tel:$this->office_number";
      $data = '';
      $data .= 'BEGIN:VCARD'."\n";
      $data .= 'VERSION:3.0'."\n";
      $data .= 'FN:'.$this->company_name."\n";
      $data .= 'TITLE:'.$this->industry."\n";
      $data .= 'EMAIL:'.$this->email_address."\n";
      $data .= 'TEL:'.$this->mobile."\n";
      $data .= 'TEL:'.$this->office_number."\n";
      $data .= 'ADDR:'.$this->address_line_1.' '.$this->country.' '.$this->postal_code."\n";
      $data .= 'URL:'.$this->facebook_link."\n";
      $data .= 'URL:'.$this->instagram_link."\n";
      $data .= 'URL:'.$this->twitter_link."\n";
      $data .= 'URL:'.$this->website."\n";
      $data .= 'END:VCARD'."\n";


      return \QrCode::size(99)->generate($data);
    }

     public function getDateEntryAttribute($value){
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }


    public function getPermissionAttribute(){
      return $this->permission()->first();
    }


    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function permission()
   {
       return $this->hasMany('App\Permission', 'company_id');
   }

     public function user()
    {
        return $this->hasMany('App\User', 'company_id');
    }

    public function mainform()
   {
       return $this->hasMany('App\MainForm', 'company_id');
   }

     public function upload()
    {
        return $this->hasMany('App\Upload', 'company_id');
    }

      public function product()
    {
        return $this->hasMany('App\Product', 'company_id');
    }

    public function form_build()
    {
        return $this->morphMany('App\FormBuild', 'formable');
    }

    public function smtp()
   {
       return $this->morphOne('App\Smtp', 'smtpable');
   }

}
