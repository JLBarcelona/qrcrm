<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use App\DailyAttendance;
use App\Payroll;
use App\User;
use App\LateConfig;
use App\Holiday;


use Carbon\Carbon;


class MonthlyExcel implements ToCollection
{
    /**
    * @param Collection $collection
    */

    function get_deduction($time){
        $late = LateConfig::whereNull('deleted_at')->get();
        foreach ($late as $data) {
            if ($time == 0) {
                return 0;
            }elseif ($time >= $data->from_time && $time <= $data->end_time) {
                return $data->deducted_from_time;
            }
        }
    }


    public function collection(Collection $collection)
    {
        $owner_id = '';
        $owner_type = '';
              

        foreach ($collection as $key => $value) {
            if ($key == 1) {
                foreach ($value as $k => $v) {
                    if ($k == 1) {
                        $detail = explode(' ~ ', $v);
                        $start = $detail[0];
                        $end = $detail[1];

                        $payroll_check = Payroll::where('report_from', $start)->where('report_to', $end);

                        if ($payroll_check->count() > 0) {
                            $pay = $payroll_check->first();
                            $owner_id = $pay->payroll_id;
                            $owner_type = get_class($pay);
                        }else{
                            $payroll = new Payroll;
                            $payroll->report_from = $start;
                            $payroll->report_to = $end;
                            $payroll->save();
                            $owner_id = $payroll->payroll_id;
                            $owner_type = get_class($payroll);
                        }


                       
                    }
                }
            }

            if ($key > 3) {
                $late_deduction = 0;
                $check_user = User::where('attendance_id', $value[0])->whereNull('deleted_at')->count();

                if ($check_user > 0) {
                    // Check holiday
                    $holiday = Holiday::where('date', $value[3])->count();
                    // Save Detail
                    $attendance = new DailyAttendance;
                    $attendance->owner_id = $owner_id; 
                    $attendance->owner_type = $owner_type;
                    $attendance->attendance_id = $value[0];
                    $attendance->date_attendance = $value[3];
                    $attendance->f_time_in = $value[4];
                    $attendance->f_time_out = $value[5];
                    $attendance->l_time_in = $value[6];
                    $attendance->l_time_out = $value[7];
                    $attendance->late_mins = $value[8];
                    $attendance->leave_early_mins = $value[9];
                    $attendance->absence_mins = $value[10];
                    $attendance->total = $value[11];
                    $attendance->note = $value[12];

                   
                    $is_holiday = ($holiday > 0)? 1 : 0;
                    $attendance->is_holiday = $is_holiday;

                     if ($value[4] == null && $value[7] == null && $is_holiday == 0) {
                        $late_deduction = $this->get_deduction($value[10]);
                    }else{
                        $late_deduction = $this->get_deduction($value[8]);
                    }

                    $attendance->deduction_basis = $late_deduction;
                    $attendance->save();
                }
                
                // foreach ($value as $new_key => $new_value) {
                //      $data[] = $new_value;
                // }                
            }
        }

    }

}
