
<title>{{ $title }}</title>
<!-- Tell the browser to be responsive to screen width -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="icon" type="icon/png" href="{{ $icon }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/theme.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/themes/twitter/twitter.css') }}">
<!-- Google Font: Source Sans Pro -->
<style type="text/css">
	body{
		font-size: 14px;
	}
</style>
<!-- If you have extra stylesheet for specific user please provide the following. Thanks! -->
@if($type == 'home')

@elseif($type == 'user')

@elseif($type == 'admin')

@elseif($type == 'store')

@elseif($type == 'register')
<link rel="stylesheet" href="{{ asset('assets/css/multistepform.css') }}">
@endif
