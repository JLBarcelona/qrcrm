 <div class="modal fade" role="dialog" id="barcode_full_screen">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        <span class="name_profile"></span>
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="text-center">
        	<img src="" alt="barcode" class="barcode_profile img-fluid" >
        </div>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>