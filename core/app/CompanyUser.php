<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model
{
  protected $primaryKey = 'company_user_id';
  protected $table = 'qrcrm_company_user';

  protected $fillable = [
      'slug','profile_picture','fullname','designation','company_id','mobile_number','address','country','postal_code','facebook_link','instagram_link','twitter_link','created_from','user_new_id','created_at','updated_at','deleted_at'
  ];

  public function owner(){
	 return $this->morphOne('App\User', 'owner')->whereNull('deleted_at');
  }

}
