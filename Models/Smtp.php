<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Smtp extends Authenticatable
{

  protected $primaryKey = 'smtp_id';

   public $timestamps = false;

   protected $table = "fcrm_smtp";

   protected $fillable = [
        'mailer_type','host','port','email','password','enc_type','smtpable_id','smtpable_type'
    ];

    public function smtpable(){
      return $this->morphTo();
    }

}
