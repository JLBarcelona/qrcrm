 <nav aria-label="breadcrumb ml-0">
    <ol class="breadcrumb ml-0" >
      @php $i = 0; $total = count($data); @endphp
      @php $link = (Auth::user()->user_type == 1)? url('admin') : url('CompanyAdmin')  @endphp

      <li class="breadcrumb-item active" aria-current="page"><a href="{{ $link }}">Home</a></li>
      @foreach($data as $key => $val)
        @php $i++;  @endphp
          @if($i == $total && $total != 1)
             <li class="breadcrumb-item active" aria-current="page">{{ $key }}</li>
          @elseif($total == 1)
            <li class="breadcrumb-item active" aria-current="page">{{ $key }}</li>
          @else
              <li class="breadcrumb-item"><a href="{{ $val }}">{{ $key }}</a></li>
          @endif
      @endforeach
    </ol>
  </nav>
