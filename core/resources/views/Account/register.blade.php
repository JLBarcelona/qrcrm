<!DOCTYPE html>
<html>
@include('Layout.header', ['type' => 'register', 'title' => 'Register', 'icon' => asset('img/logo.png') ])

<body>
  <div id="svg_wrap"></div>

      <h2>Online Application</h2>
        <form class="needs-validation" id="register_profile" action="{{ url('/register/add_register') }}" novalidate>
          <input type="hidden" id="profile_id" name="profile_id" placeholder="" class="form-control" required>
            <section>
              <label>Personal information</label>
                <div class="row">
                  <div class="col-sm-3">
                    <p>Firstname</p>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" id="firstname" name="firstname"  class="form-control " required>
        			      <div class="invalid-feedback" id="err_firstname"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3">
                    <p>Middlename</p>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" id="middlename" name="middlename" class="form-control " required>
                  <div class="invalid-feedback" id="err_middlename"></div>
                  </div>
                </div>    
                <div class="row">
                  <div class="col-sm-3">
                    <p>Lastname</p>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" id="lastname" name="lastname"  class="form-control " required>
                  <div class="invalid-feedback" id="err_lastname"></div>
                  </div>
                </div> 
                <div class="row">
                  <div class="col-sm-3">
                    <p>Birthdate</p>
                  </div>
                  <div class="col-sm-9">
                    <input type="date" id="birthdate" name="birthdate"  class="form-control " required>
                    <div class="invalid-feedback" id="err_birthdate"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3">
                    <p>Gender</p>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" id="gender" name="gender"  class="form-control " required>
                    <div class="invalid-feedback" id="err_gender"></div>
                  </div>
                </div>              
                <div class="row">
                  <div class="col-sm-3">
                    <p>Contact Number</p>
                  </div>
                  <div class="col-sm-9">
                    <input type="text" id="contact_number" name="contact_number" class="form-control " required>
                    <div class="invalid-feedback" id="err_contact_number"></div>
                  </div>
                </div>    

            </section>

            <section>
            <label>Address</label>
            <div class="row">
              <div class="col-sm-3">
                <p>House Number</p>
              </div>
              <div class="col-sm-9">
                <input type="text" id="house_number" name="house_number" class="form-control " required>
                <div class="invalid-feedback" id="err_house_number"></div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-3">
                <p>Purok/Street</p>
              </div>
              <div class="col-sm-9">
                <input type="text" id="purok_street" name="purok_street" class="form-control " required>
                <div class="invalid-feedback" id="err_purok_street"></div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-3">
                <p>Barangay</p>
              </div>
              <div class="col-sm-9">
                <input type="text" id="baranggay" name="baranggay" class="form-control " required>
                <div class="invalid-feedback" id="err_baranggay"></div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-3">
                <p>Municipality</p>
              </div>
              <div class="col-sm-9">
                <input type="text" id="municipality" name="municipality" class="form-control " required>
                <div class="invalid-feedback" id="err_municipality"></div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-3">
                <p>Postal Code</p>
              </div>
              <div class="col-sm-9">
                <input type="text" id="postal_code" name="postal_code" class="form-control " required>
                <div class="invalid-feedback" id="err_postal_code"></div>
              </div>
            </div>
            

            </section>

            <section>

            <label>Identification</label>
            <div class="row">
              <div class="col-sm-3">
                <p>Type of ID</p>
              </div>
              <div class="col-sm-9">
                <input type="text" id="type_of_id" name="type_of_id" class="form-control " required>
                <div class="invalid-feedback" id="err_type_of_id"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-3">
                <p>ID Number</p>
              </div>
              <div class="col-sm-9">
                <input type="text" id="id_number" name="id_number" class="form-control " required>
                <div class="invalid-feedback" id="err_id_number"></div>
              </div>
            </div>
            </section>
            

            <section>
             <label>Take a picture of your ID</label>

              <div id="profile_path_preview">
                  <img src="{{ asset('img/id.jpg') }}" class="img-fluid elevation-2" alt="Profile Image">
                </div>

                <div class=" mt-4">
                  <button class="btn btn-block btn-light text-primary mt-1 border" type="button" onclick="show_upload('profile_path');"><i class="fa fa-upload"></i>Upload</button>
                  
                </div>
                    
                <textarea class="hide" name="profile_path" id="profile_path"></textarea>
            </section>

            <section>

            <div id="profile_path_preview">
              <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="Profile Image">
            </div>

            <div class=" mt-4">
              <button class="btn btn-block btn-light text-primary mt-1 border" type="button" onclick="show_upload('profile_path');"><i class="fa fa-upload"></i>Upload</button>
              
            </div>
                
            <textarea class="hide" name="profile_path" id="profile_path"></textarea>

            </section>

            <section>
            <p>Terms of service</p>
            <p>By clicking Agree and send application, you agree to our Terms, Data Policy and Cookies Policy. You may receive SMS Notifications from us and can opt out any time.</p>
            </section>

            <div class="button" id="prev">&larr; Previous</div>
            <div class="button" id="next">Next &rarr;</div>
            <button type="submit" id="submitbtn">Agree and send application</button>
          </form>

</body>
  
    <div class="modal fade" role="dialog" id="modal_upload">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
              Upload
            </div>
            <a href="#" class="close" data-dismiss="modal">&times;</a>
          </div>
          <div class="modal-body">
            <input type="hidden" name="input" id="input">
            <div class="text-center">
              <div id="preview_img"></div>
            </div>
            <div class="custom-file">
              <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
              <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
            </div>
          </div>
          <div class="modal-footer text-right">
            <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
            <button class="btn btn-dark" onclick="upload();">Upload</button>
          </div>
        </div>
      </div>
    </div>

@include('Layout.footer', ['type' => 'register'])
</html>

<script>
  
  $("#register_profile").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          console.log(response)
          swal("Success", response.message, "success");
          showValidator(response.error,'register_profile');

        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'register_profile');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function show_upload(param){
    // Preview
    $("#input").val(param);
    $("#modal_upload").modal('show');
  }

  function previewFile() {
    const att = $("#input").val();
    const preview = $("#"+att+"_preview");
    const base64 = $("#"+att);
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.addEventListener("load", function () {
      base64.val(reader.result);
      $("#preview_img").html('<img src="'+reader.result+'" alt="" class="img-fluid img-thumbnail animated fadeIn mb-2">');
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }


  function upload(){
      const att = $("#input").val();
      const preview = $("#"+att+"_preview");
      const base64 = $("#"+att).val();
     preview.html('<img src="'+base64+'" alt="" class="img-fluid img-thumbnail animated fadeIn mt-2">');
     $("#modal_upload").modal('hide');
     $("#input").val('');
       $("#preview_img").html('');
  }

  function close(){
     $("#modal_upload").modal('hide');
     $("#input").val('');
       $("#preview_img").html('');
  }
 

</script>