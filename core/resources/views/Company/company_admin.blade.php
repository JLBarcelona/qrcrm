<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('asset/img/logo.png') ])
<body class="">
  <div class="">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
    <div class="">
      <section class="page-content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
                @include('Layout.bread', ['data' => [ 'Company' => url()->current() ] ])
            </div>
            <div class="col-sm-12">
              
              <div class="card">
                <div class="card-header main-headers">
                  <div class="title">Company</div>
                  <div class="options">
                    <button class="btn btn-primary btn-sm" onclick="add_new_company();"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_comp" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>


 <div class="modal fade" role="dialog" id="add_new_company">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title company">
            
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="company_form" action="{{ url('comp/add_comp') }}" novalidate>
              <div class="form-row">
                <input type="hidden" id="company_id" name="company_id" placeholder="" class="form-control" required>
                <div class="form-group col-sm-12">
                  <label>Company Name </label>
                  <input type="text" id="company_name" name="company_name" placeholder="Company Name" class="form-control " required>
                  <div class="invalid-feedback" id="err_company_name"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Username </label>
                  <input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
                  <div class="invalid-feedback" id="err_username"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Email Address </label>
                  <input type="text" id="email_address" name="email_address" placeholder="Email Address" class="form-control " required>
                  <div class="invalid-feedback" id="err_email_address"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Address Line 1 </label>
                  <input type="text" id="address_line_1" name="address_line_1" placeholder="Address Line 1" class="form-control " required>
                  <div class="invalid-feedback" id="err_address_line_1"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Address Line2 </label>
                  <input type="text" id="address_line_2" name="address_line_2" placeholder="Address Line 2" class="form-control " required>
                  <div class="invalid-feedback" id="err_address_line2"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>City </label>
                  <input type="text" id="city" name="city" placeholder="City" class="form-control " required>
                  <div class="invalid-feedback" id="err_city"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Country </label>
                  <input type="text" id="country" name="country" placeholder="Country" class="form-control " required>
                  <div class="invalid-feedback" id="err_country"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Industry </label>
                  <input type="text" id="industry" name="industry" placeholder="Industry" class="form-control " required>
                  <div class="invalid-feedback" id="err_industry"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Office Number </label>
                  <input type="text" id="office_number" name="office_number" placeholder="Office Number" class="form-control " required>
                  <div class="invalid-feedback" id="err_office_number"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Website </label>
                  <input type="text" id="website" name="website" placeholder="Website" class="form-control " required>
                  <div class="invalid-feedback" id="err_website"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Facebook Link </label>
                  <input type="text" id="facebook_link" name="facebook_link" placeholder="Facebook Link" class="form-control " required>
                  <div class="invalid-feedback" id="err_facebook_link"></div>
                </div>

                <div class="col-sm-12 text-right">
                  <button class="btn btn-dark btn-sm" type="submit">Save</button>
                </div>
              </div>
            </form>

          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<script type="text/javascript">
  function add_new_company(){
    $("#add_new_company").modal({'backdrop' : 'static'});
    $(".company").text('Add Company');
  }
</script>

<script>
  var tbl_comp;

  show_comp();

  function show_comp(){
    if (tbl_comp) {
      tbl_comp.destroy();
    }
    var url = main_path + '/comp/list_comp';
    tbl_comp = $('#tbl_comp').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    serverSide: true,
    processing: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: 'text-center width-1',
    "data": "company_id",
    "orderable": false,
    "title": "Contact QR",
     "render": function(data, type, row, meta){
        // var param_data = JSON.stringify(row);
       if (row.qrcode_detail == '' || row.qrcode_detail == null) {
          newdata1 = '<i class="fa fa-image fa-2x text-primary"></i>';
        }else{

          // console.log(row.qrcode_detail.replace('\n', ''));
          // newdata1 = '<div style="margin-top:13px; cc" onclick="show_contact(this)">'+row.qrcode+'</div>';
          newdata1 = '<img src="'+row.qrcode_detail+'" class="img_preview">';
          // $(".cc").html(row.qrcode_detail);
        }

        return newdata1;
      }
  },{
    className: 'text-center width-1',
    "data": "company_id",
    "orderable": false,
    "title": "QRCode",
     "render": function(data, type, row, meta){
        // var param_data = JSON.stringify(row);
       if (row.qrcode == '' || row.qrcode == null) {
          newdata1 = '<i class="fa fa-image fa-2x text-primary"></i>';
        }else{

          // console.log(row.qrcode_detail.replace('\n', ''));
          // newdata1 = '<div style="margin-top:13px; cc" onclick="show_contact(this)">'+row.qrcode+'</div>';
          newdata1 = '<img src="'+row.qrcode+'" class="img_preview">';
          // $(".cc").html(row.qrcode_detail);
        }

        return newdata1;
      }
  },{
    className: '',
    "data": "company_name",
    "title": "Name",
  },{
    className: '',
    "data": "owner.username",
    "title": "Username",
  },{
    className: '',
    "data": "owner.email_address",
    "title": "Email address",
  },{
    className: 'width-1 text-center',
    "data": "company_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        // console.log(row);
        newdata += '<button class="btn btn-info btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_comp(this)" type="button"><i class="fa fa-list"></i> Full Details</button>';
        newdata += ' <button class="btn btn-primary btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_comp(this)" type="button"><i class="fa fa-list"></i> Reports</button>';
        newdata += ' <button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_comp(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_comp(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
        return newdata;
      }
    }
  ]
  });
  }

  $("#company_form").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          console.log(response)
          swal("Success", response.message, "success");
          showValidator(response.error,'company_form');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'company_form');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function delete_comp(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    var url =  main_path + '/comp/delete_comp/' + data.company_id;
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this comp?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_comp(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    $('#company_id').val(data.company_id);
    $('#company_name').val(data.company_name);
    $('#username').val(data.owner.username);
    $('#email_address').val(data.owner.email_address);
    $('#address_line_1').val(data.address_line_1);
    $('#address_line2').val(data.address_line2);
    $('#city').val(data.city);
    $('#country').val(data.country);
    $('#industry').val(data.industry);
    $('#office_number').val(data.office_number);
    $('#website').val(data.website);
    $('#facebook_link').val(data.facebook_link);
    $("#add_new_company").modal({'backdrop' : 'static'});
    $(".company").text('Edit Company');
  }
</script>