<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Company;
use App\User;
use Hash;
// datatables
use DB;
use Yajra\Datatables\Datatables;

class CompanyController extends Controller
{
	 function list_comp(){
		$comp = Company::with('owner')->whereNull('deleted_at')->get();
		// return response()->json(['status' => true, 'data' => $comp]);
		return datatables()->of($comp)->make(true);
	}

	function add_comp(Request $request){
		$company_id= $request->get('company_id');
		$company_name = $request->get('company_name');
		$username = $request->get('username');
		$email_address = $request->get('email_address');
		$address_line_1 = $request->get('address_line_1');
		$address_line_2 = $request->get('address_line_2');
		$city = $request->get('city');
		$country = $request->get('country');
		$industry = $request->get('industry');
		$office_number = $request->get('office_number');
		$website = $request->get('website');
		$facebook_link = $request->get('facebook_link');

		$validator = Validator::make($request->all(), [
			'company_name' => 'required',
			'username' => 'required',
			'email_address' => 'required',
			'address_line_1' => 'required',
			'address_line_2' => 'required',
			'city' => 'required',
			'country' => 'required',
			'industry' => 'required',
			'office_number' => 'required',
			'website' => 'required',
			'facebook_link' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($company_id)) {
				$comp = Company::find($company_id);
				$comp->company_name = $company_name;
				$comp->address_line_1 = $address_line_1;
				$comp->address_line_2 = $address_line_2;
				$comp->city = $city;
				$comp->country = $country;
				$comp->industry = $industry;
				$comp->office_number = $office_number;
				$comp->website = $website;
				$comp->facebook_link = $facebook_link;
				if($comp->save()){
					return response()->json(['status' => true, 'message' => 'Company updated successfully!']);
				}
			}else{
				$comp = new Company;
				$comp->company_name = $company_name;;
				$comp->address_line_1 = $address_line_1;
				$comp->address_line_2 = $address_line_2;
				$comp->city = $city;
				$comp->country = $country;
				$comp->industry = $industry;
				$comp->office_number = $office_number;
				$comp->website = $website;
				$comp->facebook_link = $facebook_link;
				if($comp->save()){
					// Add User
					$user = new User;
					$user->username = $username;
					$user->email_address = $email_address;
					$user->password = $company_name;
					$user->owner_type = get_class($comp);
					$user->owner_id = $comp->company_id;
					if ($user->save()) {
						return response()->json(['status' => true, 'message' => 'Company saved successfully!']);
					}
				}
			}
		}
	}


	function delete_comp($company_id){
		$comp = Company::find($company_id);
		if($comp->delete()){
			return response()->json(['status' => true, 'message' => 'Company deleted successfully!']);
		}
	}
}
