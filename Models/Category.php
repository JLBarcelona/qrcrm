<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Category extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $primaryKey = 'category_id';
    protected $table = 'fcrm_product_category';

    protected $fillable = [
        'category_name', 'company_id', 'user_id', 'category_slug_id', 'date_entry','date_modified','date_deleted'
    ];

     public function product(){
        return $this->hasMany('App\Product', 'category_id', 'category');
    }

}
