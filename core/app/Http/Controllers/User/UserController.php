<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

use App\User;


use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
	function index(){
		return view('User.index');
	}

	function list_user_choice(){
		$user = User::whereNull('deleted_at')->select('user_id', 'firstname')->get();
		return $user;
	}

	function update_account(){
		return view('User.accountsettings');
	}

	// function change_password(){
	// 	return "Change Password";
	// }



	function list_user(){
	$user = User::whereNull('deleted_at')->get();
	return response()->json(['status' => true, 'data' => $user]);
	}

	function add_user(Request $request){
		$user_id= $request->get('user_id');
		$firstname = $request->get('firstname');
		$middlename = $request->get('middlename');
		$lastname = $request->get('lastname');
		$username = $request->get('username');
		$email_address = $request->get('email_address');

		$validator = Validator::make($request->all(), [
			'firstname' => 'required',
			'middlename' => 'required',
			'lastname' => 'required',
			'username' => 'required',
			'email_address' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($user_id)) {
				$user = User::find($user_id);
				$user->firstname = $firstname;
				$user->middlename = $middlename;
				$user->lastname = $lastname;
				$user->username = $username;
				$user->email_address = $email_address;
				if($user->save()){
					return response()->json(['status' => true, 'message' => 'Account details updated successfully!']);
				}
			}else{
				$user = new User;
				$user->firstname = $firstname;
				$user->middlename = $middlename;
				$user->lastname = $lastname;
				$user->username = $username;
				$user->email_address = $email_address;
				if($user->save()){
					return response()->json(['status' => true, 'message' => 'User saved successfully!']);
				}
			}
		}
	}


	function delete_user($user_id){
		$user = User::find($user_id);
		if($user->delete()){
			return response()->json(['status' => true, 'message' => 'User deleted successfully!']);
		}
	}


	function change_password(Request $request)
	{
		$user_id= $request->get('user_id');
		$currentpassword = $request->get('currentpassword');
		$newpassword = $request->get('newpassword');
		$confirmpassword = $request->get('confirmpassword');

		$validator = Validator::make($request->all(), [
			'currentpassword' => 'required',
			'newpassword' => 'required',
			'confirmpassword' => 'required'
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}
		elseif ($newpassword != $confirmpassword){
			$validator->errors()->add('confirmpassword', 'Passwords do not match');
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		 }
		 else{
			 if (!Hash::check($request['currentpassword'], Auth::user()->password)) {
		 		$validator->errors()->add('currentpassword', 'The current password is incorrect');
		 		return response()->json(['status' => false, 'error' => $validator->errors()]);
	 		}
	 		else{
	 			$user = User::find($user_id);
	 			$user->password = $newpassword;
	 			if($user->save()){
	 				return response()->json(['status' => true, 'message' => 'Password Updated Successfully']);
	 			}

	 		}
		 }
	}
}





// else{
// 		if (!Hash::check($request['currentpassword'], Auth::user()->password)) {
// 		$validator->errors()->add('currentpassword', 'The current password is incorrect!');
// 		return response()->json(['status' => false, 'error' => $validator->errors()]);
// 		}
// 		else{
// 			$user = User::find($user_id);
// 			$user->password = $newpassword;
// 			if($user->save()){
// 				return response()->json(['status' => true, 'message' => 'Password Updated Successfully']);
// 			}
// 		}
// }
