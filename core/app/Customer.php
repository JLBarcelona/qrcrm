<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $primaryKey = 'customer_id';
  protected $table = 'qrcrm_customer';

  protected $fillable = [
      'profile_picture','fullname','designation','customer_company','mobile_number','address','country','postal_code','created_at','updated_at','deleted_at','created_from','user_new_id','owner_type','owner_id'
  ];


	public function owner(){
	  return $this->morphOne('App\User', 'owner')->whereNull('deleted_at');
	}

}
