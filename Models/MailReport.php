<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class MailReport extends Authenticatable
{

  protected $primaryKey = 'send_id';

   public $timestamps = false;

   protected $table = "fcrm_mail_form_record";

   protected $fillable = [
        'main_form_id', 'user_id','date_entry'
    ];

     public function mainform()
    {
        return $this->belongsTo('App\MainForm', 'main_form_id');
    }
   

}
