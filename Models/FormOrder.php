<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class FormOrder extends Authenticatable
{

   protected $primaryKey = 'form_order_id';


   public $timestamps = false;

   protected $table = "fcrm_form_order";

   protected $appends = ['question'];

   protected $fillable = [
        'main_form_id', 'form_id', 'question_count','question_id', 'category_id'
    ];

    public function getQuestionAttribute(){
      return $this->question()->whereNull('category_id')->get();
    }


     public function mainform()
    {
        return $this->belongsTo('App\MainForm', 'main_form_id');
    }

      public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }

    public function form()
    {
        return $this->belongsTo('App\Form', 'form_id');
    }

}
