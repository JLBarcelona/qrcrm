<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $primaryKey = 'user_id';
    protected $table = 'fcrm_user';

    protected $appends = ['user_forms', 'qrcode_detail', 'my_company'];

    protected $fillable = [
        'slug_id','qr_path','username', 'password', 'name', 'profile_picture', 'designation','company_id', 'customer_company','email_address','mobile','address','country','postal_code','facebook_link','instagram_link','twitter_link','user_new_id','created_from','date_entry','date_modified','date_deleted'
    ];

    public function getQrcodeDetailAttribute(){
      $data = '';
      $data .= 'BEGIN:VCARD'."\n";
      $data .= 'VERSION:3.0'."\n";
      $data .= 'FN:'.$this->name."\n";
      $data .= 'TITLE:'.$this->designation."\n";
      $data .= 'EMAIL:'.$this->email_address."\n";
      $data .= 'TEL:'.$this->mobile."\n";
      $data .= 'ADDR:'.$this->address.' '.$this->country.' '.$this->postal_code."\n";
      $data .= 'URL:'.$this->facebook_link."\n";
      $data .= 'URL:'.$this->instagram_link."\n";
      $data .= 'URL:'.$this->twitter_link."\n";
      $data .= 'END:VCARD'."\n";

      return \QrCode::size(99)->generate($data);
    }


    public function getMyCompanyAttribute(){
      $data = $this->company()->first();
      return $data->company_name;
    }

     public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }


      public function upload()
    {
        return $this->hasMany('App\Upload', 'user_id');
    }


     public function examdata()
    {
        return $this->hasMany('App\Examdata', 'id');
    }

    public function getUserFormsAttribute(){
      return $this->purchased()->get();
    }

    public function purchased()
    {
        return $this->hasMany('App\Purchased', 'id');
    }

     public function result()
    {
        return $this->hasMany('App\Result', 'id');
    }

    public function forgot_password()
   {
       return $this->morphMany('App\ForgotPassword', 'forgotable');
   }

   public function formtake()
  {
      return $this->morphMany('App\FormTake', 'takeable');
  }

  public function smtp()
 {
     return $this->morphMany('App\Smtp', 'smtpable');
 }

}
