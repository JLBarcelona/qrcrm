<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class UserForm extends Authenticatable
{

   protected $primaryKey = 'user_form_id';


   public $timestamps = false;

   protected $table = "fcrm_user_form";

   protected $appends = ['question'];

   protected $fillable = [
        'form_order_id','purchased_id', 'main_form_id', 'form_id', 'question_id', 'id'
    ];


    public function getQuestionAttribute(){
      return $this->question()->whereNull('category_id')->get();
    }

	public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }


}
