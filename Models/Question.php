<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Question extends Authenticatable
{
   protected $primaryKey = 'question_id';

   public $timestamps = false;

   protected $table = "fcrm_questions";

   protected $fillable = [
        'form_id', 'questions', 'question_adv', 'question_type', 'question_multi', 'category_id', 'date_created'
    ];


    protected $appends = ['details', 'images'];


    public function getDetailsAttribute(){
        return $this->choice()->get();
    }

     public function getImagesAttribute(){
        return $this->image()->get();
    }


    public function category()
    {
         return $this->belongsTo('App\Category', 'category_id');
    }

     public function form()
    {
        return $this->belongsTo('App\Form', 'form_id');
    }

     public function choice()
    {
        return $this->hasMany('App\Choice', 'question_id');
    }

    public function image()
    {
        return $this->hasMany('App\Image', 'question_id');
    }

    public function getImage(){
        return $this->image()->get();
    }

    public function getQuestionChoices(){
        return $this->choice()->get();
    }

     public function examdata()
    {
        return $this->hasMany('App\Examdata', 'question_id');
    }

     public function order()
    {
        return $this->hasMany('App\FormOrder', 'question_id');
    }

     public function user_order()
    {
        return $this->hasMany('App\UserForm', 'question_id');
    }
}
