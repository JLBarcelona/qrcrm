<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class FormDetail extends Authenticatable
{

   protected $primaryKey = 'form_detail_id';


   public $timestamps = false;

   protected $table = "fcrm_form_detail";

   protected $appends = ['details'];

   protected $fillable = [
        'main_form_id', 'form_id', 'passing_mark', 'question_number','date_created','date_modified'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDetailsAttribute(){
      return $this->form()->get();
    }


     public function mainform()
    {
        return $this->belongsTo('App\MainForm', 'main_form_id');
    }

      public function form()
    {
        return $this->belongsTo('App\Form', 'form_id');
    }

}
