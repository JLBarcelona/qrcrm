<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logo.png') ])
<body class="">
  <div class="">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
    <div class="">
      <section class="page-content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">

            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>