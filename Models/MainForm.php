<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class MainForm extends Authenticatable
{

   protected $primaryKey = 'main_form_id';


   public $timestamps = false;

   protected $table = "fcrm_main_form";

   protected $appends = ['mail_reports', 'details'];


   protected $fillable = [
        'company_id','main_form_name', 'passing_mark', 'exam_timer', 'is_private','date_created', 'date_modified', 'date_deleted'
    ];

    protected $hidden = [
        'password', 'remember_token', 'status'
    ];

     // Accesor

    public function getDetailsAttribute(){
      return $this->formdetail()->get();
    }

    public function getMailReportsAttribute(){
      return $this->mailreport()->count();
    }

    public function getStatusAttribute(){
      if ($this->is_private == 1) {
        return '<button data-name="'.$this->main_form_name.'" data-id="'.$this->main_form_id.'" class="btn btn-sm btn-warning change_privacy_button" style="width:90px;">Private</button>';
      }else{
        return '<button data-name="'.$this->main_form_name.'" data-id="'.$this->main_form_id.'" class="btn btn-sm btn-info change_privacy_button" style="width:90px;">Public</button>';
      }
    }

    // Accesor

      public function formdetail()
    {
        return $this->hasMany('App\FormDetail', 'main_form_id');
    }

     public function mailreport()
    {
        return $this->hasMany('App\MailReport', 'main_form_id');
    }

    public function company()
   {
       return $this->belongsTo('App\Company', 'company_id');
   }



    public function getFormDetails(){
      return $this->formdetail()->with('form')->get();
    }

     public function item()
    {
        return $this->hasMany('App\Item', 'main_form_id');
    }

    public function getItems(){
      return $this->item()->get();
  }
    //  public function result()
    // {
    //     return $this->hasMany('App\Result', 'form_id');
    // }

   public function order()
    {
        return $this->hasMany('App\FormOrder', 'main_form_id');
    }

    public function getDateCreatedAttribute($value)
    {
        // return ucfirst($value);
        // return  \Carbon\Carbon::parse($value)->format('m-d-y h:i:s A');
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }



}
