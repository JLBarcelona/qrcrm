/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : qrcrm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2021-02-22 01:30:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for qrcrm_admin
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_admin`;
CREATE TABLE `qrcrm_admin` (
  `admin_id` int(255) NOT NULL AUTO_INCREMENT,
  `slug_id` varchar(255) DEFAULT '',
  `admin_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_admin
-- ----------------------------
INSERT INTO `qrcrm_admin` VALUES ('1', 'asdqwefa', 'JL Barcelona', '2020-10-20 11:10:00', null, null);

-- ----------------------------
-- Table structure for qrcrm_category
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_category`;
CREATE TABLE `qrcrm_category` (
  `prod_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prod_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_category
-- ----------------------------

-- ----------------------------
-- Table structure for qrcrm_company
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_company`;
CREATE TABLE `qrcrm_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `company_logo` longblob DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `industry` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `facebook_link` varchar(255) DEFAULT NULL,
  `office_number` int(11) DEFAULT NULL,
  `created_from` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_company
-- ----------------------------
INSERT INTO `qrcrm_company` VALUES ('1', null, null, '12345', '12345', '12345', '12345', null, '12345', '12345', '12345', '12345', '12345', null, null, '2021-02-21 17:29:34', '2021-02-21 17:29:34', null);

-- ----------------------------
-- Table structure for qrcrm_company_user
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_company_user`;
CREATE TABLE `qrcrm_company_user` (
  `company_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `profile_picture` longblob DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `facebook_link` varchar(255) DEFAULT NULL,
  `instagram_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_from` varchar(255) DEFAULT NULL,
  `user_new_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`company_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_company_user
-- ----------------------------

-- ----------------------------
-- Table structure for qrcrm_customer
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_customer`;
CREATE TABLE `qrcrm_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_picture` longblob DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `customer_company` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  `created_from` varchar(255) DEFAULT NULL,
  `user_new_id` int(11) DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_customer
-- ----------------------------

-- ----------------------------
-- Table structure for qrcrm_product
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_product`;
CREATE TABLE `qrcrm_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `product_price` decimal(10,0) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_product
-- ----------------------------

-- ----------------------------
-- Table structure for qrcrm_upload_files
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_upload_files`;
CREATE TABLE `qrcrm_upload_files` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` longtext DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `upload_type` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_upload_files
-- ----------------------------

-- ----------------------------
-- Table structure for qrcrm_user
-- ----------------------------
DROP TABLE IF EXISTS `qrcrm_user`;
CREATE TABLE `qrcrm_user` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `user_type` int(1) DEFAULT 2 COMMENT '1 = admin, 2 = barangay, 3 = store,  4 =profile',
  `verified` int(1) DEFAULT 0,
  `confirm` int(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrcrm_user
-- ----------------------------
INSERT INTO `qrcrm_user` VALUES ('1', '1', 'App\\Admin', 'admin', '$2y$10$6vqzXlCFA3sZNkUrecwzLu3bXCYW0PBUuGhRFioAyC6FCYDtONPqC', 'admin@gmail.com', '1', '1', '1', '2020-10-20 11:11:13', null, null);
