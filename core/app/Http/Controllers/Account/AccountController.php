<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

use App\User;
use App\Profile;
use Storage;



class AccountController extends Controller
{

  function register_index () {

      $json = Storage::disk('local')->get('ph.json');
      $json = json_decode($json, true);

      // dd($json);
      return view ('Account.register');
    }

  function verify_account(Request $request, $token, $id){
    $check = User::where('verification_code', $token)->where('user_id', $id)->whereNull('verified_at');
    if ($check->count() > 0) {
      $verify = $check->first();
      $verify->verified_at = now();

      if ($verify->save()) {
        return view('Account.verified', compact('verify'));
      }

    }else{
        return view('Account.login');
    }
  }



   function login_user(Request $request){

    	$result = array();


    	$user_login = array(
    		'email_address' => $request->get('email_address'),
    		'password' => $request->get('password')
    	);


    		$validator = Validator::make($request->all(), [
              'email_address' => 'required',
              'password' => 'required',
              // 'g-recaptcha-response' => 'required'
        ]);



		if ($validator->fails()) {
			 return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{

	      	if (Auth::attempt($user_login)) {

	          $user_type = Auth::user()->user_type;
            $verified = Auth::user()->verified;

             if ($user_type == 1) {
               $urls = url('admin');
              }else if ($user_type == 2) {
               $urls = url('company');
              }else if ($user_type == 3) {
               $urls = url('user');
              }else if ($user_type == 4) {
               $urls = url('customer');
              }

            if (!empty($verified) || $verified !== 0) {
              return response()->json(['status' => true, 'message' => 'Accept!', 'url' => $urls]);
            }else{
              $validator->errors()->add('email_address', 'Please verify your account to your email!');
              // $validator->errors()->add('password', 'Please verify your  to your email!');
              return response()->json(['status' => false, 'error' => $validator->errors()]);
            }

          }else{
	            $validator->errors()->add('email_address', 'Invalid Account!');
	            $validator->errors()->add('password', 'Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
	      }
	}

  }

    function logout_admin(){
        auth()->guard('admin')->logout();
        return redirect('/');
    }

    function logout_user(){
        Auth::logout();
        return redirect('/');
    }

     public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        }
    }



    function add_profile(Request $request){
      	$profile_id= $request->get('profile_id');
      	$firstname = $request->get('firstname');
      	$middlename = $request->get('middlename');
      	$lastname = $request->get('lastname');
      	$birthdate = $request->get('birthdate');
      	$gender = $request->get('gender');
      	$municipality = $request->get('municipality');
      	$barangay = $request->get('barangay');
      	$purok_street = $request->get('purok_street');
      	$house_number = $request->get('house_number');
      	$postal_code = $request->get('postal_code');
      	$contact_number = $request->get('contact_number');

      	$validator = Validator::make($request->all(), [
      		'firstname' => 'required',
      		'middlename' => 'required',
      		'lastname' => 'required',
      		'birthdate' => 'required',
      		'gender' => 'required',
      		'municipality' => 'required',
      		'barangay' => 'required',
      		'purok_street' => 'required',
      		'house_number' => 'required',
      		'postal_code' => 'required',
      		'contact_number' => 'required',
      	]);

      	if ($validator->fails()) {
      		return response()->json(['status' => false, 'error' => $validator->errors()]);
      	}else{
      		if (!empty($profile_id)) {
      			$profile = Profile::find($profile_id);
      			$profile->firstname = $firstname;
      			$profile->middlename = $middlename;
      			$profile->lastname = $lastname;
      			$profile->birthdate = $birthdate;
      			$profile->gender = $gender;
      			$profile->municipality = $municipality;
      			$profile->barangay = $barangay;
      			$profile->purok_street = $purok_street;
      			$profile->house_number = $house_number;
      			$profile->postal_code = $postal_code;
      			$profile->contact_number = $contact_number;
      			if($profile->save()){
      				return response()->json(['status' => true, 'message' => 'Profile updated successfully!']);
      			}
      		}else{
      			$profile = new Profile;
      			$profile->firstname = $firstname;
      			$profile->middlename = $middlename;
      			$profile->lastname = $lastname;
      			$profile->birthdate = $birthdate;
      			$profile->gender = $gender;
      			$profile->municipality = $municipality;
      			$profile->barangay = $barangay;
      			$profile->purok_street = $purok_street;
      			$profile->house_number = $house_number;
      			$profile->postal_code = $postal_code;
      			$profile->contact_number = $contact_number;
      			if($profile->save()){
      				return response()->json(['status' => true, 'message' => 'Profile saved successfully!']);
      			}
      		}
      	}
      }

      function add_register(Request $request){
        $profile_id= $request->get('profile_id');
        $firstname = $request->get('firstname');
        $middlename = $request->get('middlename');
        $lastname = $request->get('lastname');
        $birthdate = $request->get('birthdate');
        $gender = $request->get('gender');
        $municipality = $request->get('municipality');
        $baranggay = $request->get('baranggay');
        $purok_street = $request->get('purok_street');
        $house_number = $request->get('house_number');
        $postal_code = $request->get('postal_code');
        $contact_number = $request->get('contact_number');
        $type_of_id = $request->get('type_of_id');
        $id_number = $request->get('id_number');

        $validator = Validator::make($request->all(), [
          'firstname' => 'required',
          'middlename' => 'required',
          'lastname' => 'required',
          'birthdate' => 'required',
          'gender' => 'required',
          'municipality' => 'required',
          'baranggay' => 'required',
          'purok_street' => 'required',
          'house_number' => 'required',
          'postal_code' => 'required',
          'contact_number' => 'required',
          'type_of_id' => 'required',
          'id_number' => 'required'
        ]);

        
            $profile = new Profile;
            $profile->firstname = $firstname;
            $profile->middlename = $middlename;
            $profile->lastname = $lastname;
            $profile->birthdate = $birthdate;
            $profile->gender = $gender;
            $profile->municipality = $municipality;
            $profile->baranggay = $baranggay;
            $profile->purok_street = $purok_street;
            $profile->house_number = $house_number;
            $profile->postal_code = $postal_code;
            $profile->contact_number = $contact_number;
            $profile->type_of_id = $type_of_id;
            $profile->id_number = $id_number;
            if($profile->save()){
              return response()->json(['status' => true, 'message' => 'Profile saved successfully!','redirect'=>['route'=>'Account.index']]);
              
            }
          
        }
      
        

}
