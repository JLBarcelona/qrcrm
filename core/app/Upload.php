<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $primaryKey = 'file_id';
    protected $table = 'qrcrm_upload_files';

    protected $fillable = [
      'file_name','file_type','upload_type','created_at','updated_at','deleted_at'
    ];

    protected $hidden = [ 'owner_type','owner_id' ];


    public function owner()
    {
        return $this->morphTo();
    }
}
