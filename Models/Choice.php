<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Choice extends Authenticatable
{

  protected $primaryKey = 'choice_id';

   public $timestamps = false;

   protected $table = "fcrm_choices";

   protected $fillable = [
        'question_id', 'ordering','choices', 'points'
    ];
   

    protected $hidden = [
        'password', 'remember_token',
    ];

     public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }

    public function examdata()
    {
        return $this->hasMany('App\Examdata', 'choice_id');
    }
}
