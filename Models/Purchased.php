<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Purchased extends Authenticatable
{

  protected $primaryKey = 'purchased_id';

   public $timestamps = false;

   protected $table = "fcrm_purchased";

   protected $fillable = [
        'token', 'id', 'item_id', 'form_timer','expiration_date', 'date_finished', 'date_purchased', 'date_modified', 'date_deleted'
    ];


      public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    public function result()
    {
        return $this->hasMany('App\Result', 'purchased_id');
    }


    public function user()
  {
      return $this->belongsTo('App\User', 'id');
  }


}
